package org.com.testpro.entity;

import java.util.Date;

public class BfBugInfo {
    private Integer id;

    private Date createdAt;

    private Integer createdBy;

    private Date updatedAt;

    private Integer updatedBy;

    private String bugStatus;

    private Integer assignTo;

    private String title;

    private Short lockVersion;

    private Date resolvedAt;

    private Integer resolvedBy;

    private Date closedAt;

    private Integer closedBy;

    private String relatedBug;

    private String relatedCase;

    private String relatedResult;

    private Integer productmoduleId;

    private String solution;

    private String duplicateId;

    private Integer productId;

    private Integer reopenCount;

    private Byte priority;

    private Byte severity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getBugStatus() {
        return bugStatus;
    }

    public void setBugStatus(String bugStatus) {
        this.bugStatus = bugStatus == null ? null : bugStatus.trim();
    }

    public Integer getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(Integer assignTo) {
        this.assignTo = assignTo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Short getLockVersion() {
        return lockVersion;
    }

    public void setLockVersion(Short lockVersion) {
        this.lockVersion = lockVersion;
    }

    public Date getResolvedAt() {
        return resolvedAt;
    }

    public void setResolvedAt(Date resolvedAt) {
        this.resolvedAt = resolvedAt;
    }

    public Integer getResolvedBy() {
        return resolvedBy;
    }

    public void setResolvedBy(Integer resolvedBy) {
        this.resolvedBy = resolvedBy;
    }

    public Date getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(Date closedAt) {
        this.closedAt = closedAt;
    }

    public Integer getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(Integer closedBy) {
        this.closedBy = closedBy;
    }

    public String getRelatedBug() {
        return relatedBug;
    }

    public void setRelatedBug(String relatedBug) {
        this.relatedBug = relatedBug == null ? null : relatedBug.trim();
    }

    public String getRelatedCase() {
        return relatedCase;
    }

    public void setRelatedCase(String relatedCase) {
        this.relatedCase = relatedCase == null ? null : relatedCase.trim();
    }

    public String getRelatedResult() {
        return relatedResult;
    }

    public void setRelatedResult(String relatedResult) {
        this.relatedResult = relatedResult == null ? null : relatedResult.trim();
    }

    public Integer getProductmoduleId() {
        return productmoduleId;
    }

    public void setProductmoduleId(Integer productmoduleId) {
        this.productmoduleId = productmoduleId;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution == null ? null : solution.trim();
    }

    public String getDuplicateId() {
        return duplicateId;
    }

    public void setDuplicateId(String duplicateId) {
        this.duplicateId = duplicateId == null ? null : duplicateId.trim();
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getReopenCount() {
        return reopenCount;
    }

    public void setReopenCount(Integer reopenCount) {
        this.reopenCount = reopenCount;
    }

    public Byte getPriority() {
        return priority;
    }

    public void setPriority(Byte priority) {
        this.priority = priority;
    }

    public Byte getSeverity() {
        return severity;
    }

    public void setSeverity(Byte severity) {
        this.severity = severity;
    }
}