package org.com.testpro.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BfBugInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BfBugInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("created_at is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("created_at is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Date value) {
            addCriterion("created_at =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Date value) {
            addCriterion("created_at <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Date value) {
            addCriterion("created_at >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Date value) {
            addCriterion("created_at >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Date value) {
            addCriterion("created_at <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Date value) {
            addCriterion("created_at <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Date> values) {
            addCriterion("created_at in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Date> values) {
            addCriterion("created_at not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Date value1, Date value2) {
            addCriterion("created_at between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Date value1, Date value2) {
            addCriterion("created_at not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedByIsNull() {
            addCriterion("created_by is null");
            return (Criteria) this;
        }

        public Criteria andCreatedByIsNotNull() {
            addCriterion("created_by is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedByEqualTo(Integer value) {
            addCriterion("created_by =", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByNotEqualTo(Integer value) {
            addCriterion("created_by <>", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByGreaterThan(Integer value) {
            addCriterion("created_by >", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByGreaterThanOrEqualTo(Integer value) {
            addCriterion("created_by >=", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByLessThan(Integer value) {
            addCriterion("created_by <", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByLessThanOrEqualTo(Integer value) {
            addCriterion("created_by <=", value, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByIn(List<Integer> values) {
            addCriterion("created_by in", values, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByNotIn(List<Integer> values) {
            addCriterion("created_by not in", values, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByBetween(Integer value1, Integer value2) {
            addCriterion("created_by between", value1, value2, "createdBy");
            return (Criteria) this;
        }

        public Criteria andCreatedByNotBetween(Integer value1, Integer value2) {
            addCriterion("created_by not between", value1, value2, "createdBy");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("updated_at is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("updated_at is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Date value) {
            addCriterion("updated_at =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Date value) {
            addCriterion("updated_at <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Date value) {
            addCriterion("updated_at >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Date value) {
            addCriterion("updated_at >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Date value) {
            addCriterion("updated_at <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Date value) {
            addCriterion("updated_at <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Date> values) {
            addCriterion("updated_at in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Date> values) {
            addCriterion("updated_at not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Date value1, Date value2) {
            addCriterion("updated_at between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Date value1, Date value2) {
            addCriterion("updated_at not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedByIsNull() {
            addCriterion("updated_by is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedByIsNotNull() {
            addCriterion("updated_by is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedByEqualTo(Integer value) {
            addCriterion("updated_by =", value, "updatedBy");
            return (Criteria) this;
        }

        public Criteria andUpdatedByNotEqualTo(Integer value) {
            addCriterion("updated_by <>", value, "updatedBy");
            return (Criteria) this;
        }

        public Criteria andUpdatedByGreaterThan(Integer value) {
            addCriterion("updated_by >", value, "updatedBy");
            return (Criteria) this;
        }

        public Criteria andUpdatedByGreaterThanOrEqualTo(Integer value) {
            addCriterion("updated_by >=", value, "updatedBy");
            return (Criteria) this;
        }

        public Criteria andUpdatedByLessThan(Integer value) {
            addCriterion("updated_by <", value, "updatedBy");
            return (Criteria) this;
        }

        public Criteria andUpdatedByLessThanOrEqualTo(Integer value) {
            addCriterion("updated_by <=", value, "updatedBy");
            return (Criteria) this;
        }

        public Criteria andUpdatedByIn(List<Integer> values) {
            addCriterion("updated_by in", values, "updatedBy");
            return (Criteria) this;
        }

        public Criteria andUpdatedByNotIn(List<Integer> values) {
            addCriterion("updated_by not in", values, "updatedBy");
            return (Criteria) this;
        }

        public Criteria andUpdatedByBetween(Integer value1, Integer value2) {
            addCriterion("updated_by between", value1, value2, "updatedBy");
            return (Criteria) this;
        }

        public Criteria andUpdatedByNotBetween(Integer value1, Integer value2) {
            addCriterion("updated_by not between", value1, value2, "updatedBy");
            return (Criteria) this;
        }

        public Criteria andBugStatusIsNull() {
            addCriterion("bug_status is null");
            return (Criteria) this;
        }

        public Criteria andBugStatusIsNotNull() {
            addCriterion("bug_status is not null");
            return (Criteria) this;
        }

        public Criteria andBugStatusEqualTo(String value) {
            addCriterion("bug_status =", value, "bugStatus");
            return (Criteria) this;
        }

        public Criteria andBugStatusNotEqualTo(String value) {
            addCriterion("bug_status <>", value, "bugStatus");
            return (Criteria) this;
        }

        public Criteria andBugStatusGreaterThan(String value) {
            addCriterion("bug_status >", value, "bugStatus");
            return (Criteria) this;
        }

        public Criteria andBugStatusGreaterThanOrEqualTo(String value) {
            addCriterion("bug_status >=", value, "bugStatus");
            return (Criteria) this;
        }

        public Criteria andBugStatusLessThan(String value) {
            addCriterion("bug_status <", value, "bugStatus");
            return (Criteria) this;
        }

        public Criteria andBugStatusLessThanOrEqualTo(String value) {
            addCriterion("bug_status <=", value, "bugStatus");
            return (Criteria) this;
        }

        public Criteria andBugStatusLike(String value) {
            addCriterion("bug_status like", value, "bugStatus");
            return (Criteria) this;
        }

        public Criteria andBugStatusNotLike(String value) {
            addCriterion("bug_status not like", value, "bugStatus");
            return (Criteria) this;
        }

        public Criteria andBugStatusIn(List<String> values) {
            addCriterion("bug_status in", values, "bugStatus");
            return (Criteria) this;
        }

        public Criteria andBugStatusNotIn(List<String> values) {
            addCriterion("bug_status not in", values, "bugStatus");
            return (Criteria) this;
        }

        public Criteria andBugStatusBetween(String value1, String value2) {
            addCriterion("bug_status between", value1, value2, "bugStatus");
            return (Criteria) this;
        }

        public Criteria andBugStatusNotBetween(String value1, String value2) {
            addCriterion("bug_status not between", value1, value2, "bugStatus");
            return (Criteria) this;
        }

        public Criteria andAssignToIsNull() {
            addCriterion("assign_to is null");
            return (Criteria) this;
        }

        public Criteria andAssignToIsNotNull() {
            addCriterion("assign_to is not null");
            return (Criteria) this;
        }

        public Criteria andAssignToEqualTo(Integer value) {
            addCriterion("assign_to =", value, "assignTo");
            return (Criteria) this;
        }

        public Criteria andAssignToNotEqualTo(Integer value) {
            addCriterion("assign_to <>", value, "assignTo");
            return (Criteria) this;
        }

        public Criteria andAssignToGreaterThan(Integer value) {
            addCriterion("assign_to >", value, "assignTo");
            return (Criteria) this;
        }

        public Criteria andAssignToGreaterThanOrEqualTo(Integer value) {
            addCriterion("assign_to >=", value, "assignTo");
            return (Criteria) this;
        }

        public Criteria andAssignToLessThan(Integer value) {
            addCriterion("assign_to <", value, "assignTo");
            return (Criteria) this;
        }

        public Criteria andAssignToLessThanOrEqualTo(Integer value) {
            addCriterion("assign_to <=", value, "assignTo");
            return (Criteria) this;
        }

        public Criteria andAssignToIn(List<Integer> values) {
            addCriterion("assign_to in", values, "assignTo");
            return (Criteria) this;
        }

        public Criteria andAssignToNotIn(List<Integer> values) {
            addCriterion("assign_to not in", values, "assignTo");
            return (Criteria) this;
        }

        public Criteria andAssignToBetween(Integer value1, Integer value2) {
            addCriterion("assign_to between", value1, value2, "assignTo");
            return (Criteria) this;
        }

        public Criteria andAssignToNotBetween(Integer value1, Integer value2) {
            addCriterion("assign_to not between", value1, value2, "assignTo");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("title is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("title is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("title =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("title <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("title >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("title >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("title <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("title <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("title like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("title not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("title in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("title not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("title between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("title not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andLockVersionIsNull() {
            addCriterion("lock_version is null");
            return (Criteria) this;
        }

        public Criteria andLockVersionIsNotNull() {
            addCriterion("lock_version is not null");
            return (Criteria) this;
        }

        public Criteria andLockVersionEqualTo(Short value) {
            addCriterion("lock_version =", value, "lockVersion");
            return (Criteria) this;
        }

        public Criteria andLockVersionNotEqualTo(Short value) {
            addCriterion("lock_version <>", value, "lockVersion");
            return (Criteria) this;
        }

        public Criteria andLockVersionGreaterThan(Short value) {
            addCriterion("lock_version >", value, "lockVersion");
            return (Criteria) this;
        }

        public Criteria andLockVersionGreaterThanOrEqualTo(Short value) {
            addCriterion("lock_version >=", value, "lockVersion");
            return (Criteria) this;
        }

        public Criteria andLockVersionLessThan(Short value) {
            addCriterion("lock_version <", value, "lockVersion");
            return (Criteria) this;
        }

        public Criteria andLockVersionLessThanOrEqualTo(Short value) {
            addCriterion("lock_version <=", value, "lockVersion");
            return (Criteria) this;
        }

        public Criteria andLockVersionIn(List<Short> values) {
            addCriterion("lock_version in", values, "lockVersion");
            return (Criteria) this;
        }

        public Criteria andLockVersionNotIn(List<Short> values) {
            addCriterion("lock_version not in", values, "lockVersion");
            return (Criteria) this;
        }

        public Criteria andLockVersionBetween(Short value1, Short value2) {
            addCriterion("lock_version between", value1, value2, "lockVersion");
            return (Criteria) this;
        }

        public Criteria andLockVersionNotBetween(Short value1, Short value2) {
            addCriterion("lock_version not between", value1, value2, "lockVersion");
            return (Criteria) this;
        }

        public Criteria andResolvedAtIsNull() {
            addCriterion("resolved_at is null");
            return (Criteria) this;
        }

        public Criteria andResolvedAtIsNotNull() {
            addCriterion("resolved_at is not null");
            return (Criteria) this;
        }

        public Criteria andResolvedAtEqualTo(Date value) {
            addCriterion("resolved_at =", value, "resolvedAt");
            return (Criteria) this;
        }

        public Criteria andResolvedAtNotEqualTo(Date value) {
            addCriterion("resolved_at <>", value, "resolvedAt");
            return (Criteria) this;
        }

        public Criteria andResolvedAtGreaterThan(Date value) {
            addCriterion("resolved_at >", value, "resolvedAt");
            return (Criteria) this;
        }

        public Criteria andResolvedAtGreaterThanOrEqualTo(Date value) {
            addCriterion("resolved_at >=", value, "resolvedAt");
            return (Criteria) this;
        }

        public Criteria andResolvedAtLessThan(Date value) {
            addCriterion("resolved_at <", value, "resolvedAt");
            return (Criteria) this;
        }

        public Criteria andResolvedAtLessThanOrEqualTo(Date value) {
            addCriterion("resolved_at <=", value, "resolvedAt");
            return (Criteria) this;
        }

        public Criteria andResolvedAtIn(List<Date> values) {
            addCriterion("resolved_at in", values, "resolvedAt");
            return (Criteria) this;
        }

        public Criteria andResolvedAtNotIn(List<Date> values) {
            addCriterion("resolved_at not in", values, "resolvedAt");
            return (Criteria) this;
        }

        public Criteria andResolvedAtBetween(Date value1, Date value2) {
            addCriterion("resolved_at between", value1, value2, "resolvedAt");
            return (Criteria) this;
        }

        public Criteria andResolvedAtNotBetween(Date value1, Date value2) {
            addCriterion("resolved_at not between", value1, value2, "resolvedAt");
            return (Criteria) this;
        }

        public Criteria andResolvedByIsNull() {
            addCriterion("resolved_by is null");
            return (Criteria) this;
        }

        public Criteria andResolvedByIsNotNull() {
            addCriterion("resolved_by is not null");
            return (Criteria) this;
        }

        public Criteria andResolvedByEqualTo(Integer value) {
            addCriterion("resolved_by =", value, "resolvedBy");
            return (Criteria) this;
        }

        public Criteria andResolvedByNotEqualTo(Integer value) {
            addCriterion("resolved_by <>", value, "resolvedBy");
            return (Criteria) this;
        }

        public Criteria andResolvedByGreaterThan(Integer value) {
            addCriterion("resolved_by >", value, "resolvedBy");
            return (Criteria) this;
        }

        public Criteria andResolvedByGreaterThanOrEqualTo(Integer value) {
            addCriterion("resolved_by >=", value, "resolvedBy");
            return (Criteria) this;
        }

        public Criteria andResolvedByLessThan(Integer value) {
            addCriterion("resolved_by <", value, "resolvedBy");
            return (Criteria) this;
        }

        public Criteria andResolvedByLessThanOrEqualTo(Integer value) {
            addCriterion("resolved_by <=", value, "resolvedBy");
            return (Criteria) this;
        }

        public Criteria andResolvedByIn(List<Integer> values) {
            addCriterion("resolved_by in", values, "resolvedBy");
            return (Criteria) this;
        }

        public Criteria andResolvedByNotIn(List<Integer> values) {
            addCriterion("resolved_by not in", values, "resolvedBy");
            return (Criteria) this;
        }

        public Criteria andResolvedByBetween(Integer value1, Integer value2) {
            addCriterion("resolved_by between", value1, value2, "resolvedBy");
            return (Criteria) this;
        }

        public Criteria andResolvedByNotBetween(Integer value1, Integer value2) {
            addCriterion("resolved_by not between", value1, value2, "resolvedBy");
            return (Criteria) this;
        }

        public Criteria andClosedAtIsNull() {
            addCriterion("closed_at is null");
            return (Criteria) this;
        }

        public Criteria andClosedAtIsNotNull() {
            addCriterion("closed_at is not null");
            return (Criteria) this;
        }

        public Criteria andClosedAtEqualTo(Date value) {
            addCriterion("closed_at =", value, "closedAt");
            return (Criteria) this;
        }

        public Criteria andClosedAtNotEqualTo(Date value) {
            addCriterion("closed_at <>", value, "closedAt");
            return (Criteria) this;
        }

        public Criteria andClosedAtGreaterThan(Date value) {
            addCriterion("closed_at >", value, "closedAt");
            return (Criteria) this;
        }

        public Criteria andClosedAtGreaterThanOrEqualTo(Date value) {
            addCriterion("closed_at >=", value, "closedAt");
            return (Criteria) this;
        }

        public Criteria andClosedAtLessThan(Date value) {
            addCriterion("closed_at <", value, "closedAt");
            return (Criteria) this;
        }

        public Criteria andClosedAtLessThanOrEqualTo(Date value) {
            addCriterion("closed_at <=", value, "closedAt");
            return (Criteria) this;
        }

        public Criteria andClosedAtIn(List<Date> values) {
            addCriterion("closed_at in", values, "closedAt");
            return (Criteria) this;
        }

        public Criteria andClosedAtNotIn(List<Date> values) {
            addCriterion("closed_at not in", values, "closedAt");
            return (Criteria) this;
        }

        public Criteria andClosedAtBetween(Date value1, Date value2) {
            addCriterion("closed_at between", value1, value2, "closedAt");
            return (Criteria) this;
        }

        public Criteria andClosedAtNotBetween(Date value1, Date value2) {
            addCriterion("closed_at not between", value1, value2, "closedAt");
            return (Criteria) this;
        }

        public Criteria andClosedByIsNull() {
            addCriterion("closed_by is null");
            return (Criteria) this;
        }

        public Criteria andClosedByIsNotNull() {
            addCriterion("closed_by is not null");
            return (Criteria) this;
        }

        public Criteria andClosedByEqualTo(Integer value) {
            addCriterion("closed_by =", value, "closedBy");
            return (Criteria) this;
        }

        public Criteria andClosedByNotEqualTo(Integer value) {
            addCriterion("closed_by <>", value, "closedBy");
            return (Criteria) this;
        }

        public Criteria andClosedByGreaterThan(Integer value) {
            addCriterion("closed_by >", value, "closedBy");
            return (Criteria) this;
        }

        public Criteria andClosedByGreaterThanOrEqualTo(Integer value) {
            addCriterion("closed_by >=", value, "closedBy");
            return (Criteria) this;
        }

        public Criteria andClosedByLessThan(Integer value) {
            addCriterion("closed_by <", value, "closedBy");
            return (Criteria) this;
        }

        public Criteria andClosedByLessThanOrEqualTo(Integer value) {
            addCriterion("closed_by <=", value, "closedBy");
            return (Criteria) this;
        }

        public Criteria andClosedByIn(List<Integer> values) {
            addCriterion("closed_by in", values, "closedBy");
            return (Criteria) this;
        }

        public Criteria andClosedByNotIn(List<Integer> values) {
            addCriterion("closed_by not in", values, "closedBy");
            return (Criteria) this;
        }

        public Criteria andClosedByBetween(Integer value1, Integer value2) {
            addCriterion("closed_by between", value1, value2, "closedBy");
            return (Criteria) this;
        }

        public Criteria andClosedByNotBetween(Integer value1, Integer value2) {
            addCriterion("closed_by not between", value1, value2, "closedBy");
            return (Criteria) this;
        }

        public Criteria andRelatedBugIsNull() {
            addCriterion("related_bug is null");
            return (Criteria) this;
        }

        public Criteria andRelatedBugIsNotNull() {
            addCriterion("related_bug is not null");
            return (Criteria) this;
        }

        public Criteria andRelatedBugEqualTo(String value) {
            addCriterion("related_bug =", value, "relatedBug");
            return (Criteria) this;
        }

        public Criteria andRelatedBugNotEqualTo(String value) {
            addCriterion("related_bug <>", value, "relatedBug");
            return (Criteria) this;
        }

        public Criteria andRelatedBugGreaterThan(String value) {
            addCriterion("related_bug >", value, "relatedBug");
            return (Criteria) this;
        }

        public Criteria andRelatedBugGreaterThanOrEqualTo(String value) {
            addCriterion("related_bug >=", value, "relatedBug");
            return (Criteria) this;
        }

        public Criteria andRelatedBugLessThan(String value) {
            addCriterion("related_bug <", value, "relatedBug");
            return (Criteria) this;
        }

        public Criteria andRelatedBugLessThanOrEqualTo(String value) {
            addCriterion("related_bug <=", value, "relatedBug");
            return (Criteria) this;
        }

        public Criteria andRelatedBugLike(String value) {
            addCriterion("related_bug like", value, "relatedBug");
            return (Criteria) this;
        }

        public Criteria andRelatedBugNotLike(String value) {
            addCriterion("related_bug not like", value, "relatedBug");
            return (Criteria) this;
        }

        public Criteria andRelatedBugIn(List<String> values) {
            addCriterion("related_bug in", values, "relatedBug");
            return (Criteria) this;
        }

        public Criteria andRelatedBugNotIn(List<String> values) {
            addCriterion("related_bug not in", values, "relatedBug");
            return (Criteria) this;
        }

        public Criteria andRelatedBugBetween(String value1, String value2) {
            addCriterion("related_bug between", value1, value2, "relatedBug");
            return (Criteria) this;
        }

        public Criteria andRelatedBugNotBetween(String value1, String value2) {
            addCriterion("related_bug not between", value1, value2, "relatedBug");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseIsNull() {
            addCriterion("related_case is null");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseIsNotNull() {
            addCriterion("related_case is not null");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseEqualTo(String value) {
            addCriterion("related_case =", value, "relatedCase");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseNotEqualTo(String value) {
            addCriterion("related_case <>", value, "relatedCase");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseGreaterThan(String value) {
            addCriterion("related_case >", value, "relatedCase");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseGreaterThanOrEqualTo(String value) {
            addCriterion("related_case >=", value, "relatedCase");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseLessThan(String value) {
            addCriterion("related_case <", value, "relatedCase");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseLessThanOrEqualTo(String value) {
            addCriterion("related_case <=", value, "relatedCase");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseLike(String value) {
            addCriterion("related_case like", value, "relatedCase");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseNotLike(String value) {
            addCriterion("related_case not like", value, "relatedCase");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseIn(List<String> values) {
            addCriterion("related_case in", values, "relatedCase");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseNotIn(List<String> values) {
            addCriterion("related_case not in", values, "relatedCase");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseBetween(String value1, String value2) {
            addCriterion("related_case between", value1, value2, "relatedCase");
            return (Criteria) this;
        }

        public Criteria andRelatedCaseNotBetween(String value1, String value2) {
            addCriterion("related_case not between", value1, value2, "relatedCase");
            return (Criteria) this;
        }

        public Criteria andRelatedResultIsNull() {
            addCriterion("related_result is null");
            return (Criteria) this;
        }

        public Criteria andRelatedResultIsNotNull() {
            addCriterion("related_result is not null");
            return (Criteria) this;
        }

        public Criteria andRelatedResultEqualTo(String value) {
            addCriterion("related_result =", value, "relatedResult");
            return (Criteria) this;
        }

        public Criteria andRelatedResultNotEqualTo(String value) {
            addCriterion("related_result <>", value, "relatedResult");
            return (Criteria) this;
        }

        public Criteria andRelatedResultGreaterThan(String value) {
            addCriterion("related_result >", value, "relatedResult");
            return (Criteria) this;
        }

        public Criteria andRelatedResultGreaterThanOrEqualTo(String value) {
            addCriterion("related_result >=", value, "relatedResult");
            return (Criteria) this;
        }

        public Criteria andRelatedResultLessThan(String value) {
            addCriterion("related_result <", value, "relatedResult");
            return (Criteria) this;
        }

        public Criteria andRelatedResultLessThanOrEqualTo(String value) {
            addCriterion("related_result <=", value, "relatedResult");
            return (Criteria) this;
        }

        public Criteria andRelatedResultLike(String value) {
            addCriterion("related_result like", value, "relatedResult");
            return (Criteria) this;
        }

        public Criteria andRelatedResultNotLike(String value) {
            addCriterion("related_result not like", value, "relatedResult");
            return (Criteria) this;
        }

        public Criteria andRelatedResultIn(List<String> values) {
            addCriterion("related_result in", values, "relatedResult");
            return (Criteria) this;
        }

        public Criteria andRelatedResultNotIn(List<String> values) {
            addCriterion("related_result not in", values, "relatedResult");
            return (Criteria) this;
        }

        public Criteria andRelatedResultBetween(String value1, String value2) {
            addCriterion("related_result between", value1, value2, "relatedResult");
            return (Criteria) this;
        }

        public Criteria andRelatedResultNotBetween(String value1, String value2) {
            addCriterion("related_result not between", value1, value2, "relatedResult");
            return (Criteria) this;
        }

        public Criteria andProductmoduleIdIsNull() {
            addCriterion("productmodule_id is null");
            return (Criteria) this;
        }

        public Criteria andProductmoduleIdIsNotNull() {
            addCriterion("productmodule_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductmoduleIdEqualTo(Integer value) {
            addCriterion("productmodule_id =", value, "productmoduleId");
            return (Criteria) this;
        }

        public Criteria andProductmoduleIdNotEqualTo(Integer value) {
            addCriterion("productmodule_id <>", value, "productmoduleId");
            return (Criteria) this;
        }

        public Criteria andProductmoduleIdGreaterThan(Integer value) {
            addCriterion("productmodule_id >", value, "productmoduleId");
            return (Criteria) this;
        }

        public Criteria andProductmoduleIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("productmodule_id >=", value, "productmoduleId");
            return (Criteria) this;
        }

        public Criteria andProductmoduleIdLessThan(Integer value) {
            addCriterion("productmodule_id <", value, "productmoduleId");
            return (Criteria) this;
        }

        public Criteria andProductmoduleIdLessThanOrEqualTo(Integer value) {
            addCriterion("productmodule_id <=", value, "productmoduleId");
            return (Criteria) this;
        }

        public Criteria andProductmoduleIdIn(List<Integer> values) {
            addCriterion("productmodule_id in", values, "productmoduleId");
            return (Criteria) this;
        }

        public Criteria andProductmoduleIdNotIn(List<Integer> values) {
            addCriterion("productmodule_id not in", values, "productmoduleId");
            return (Criteria) this;
        }

        public Criteria andProductmoduleIdBetween(Integer value1, Integer value2) {
            addCriterion("productmodule_id between", value1, value2, "productmoduleId");
            return (Criteria) this;
        }

        public Criteria andProductmoduleIdNotBetween(Integer value1, Integer value2) {
            addCriterion("productmodule_id not between", value1, value2, "productmoduleId");
            return (Criteria) this;
        }

        public Criteria andSolutionIsNull() {
            addCriterion("solution is null");
            return (Criteria) this;
        }

        public Criteria andSolutionIsNotNull() {
            addCriterion("solution is not null");
            return (Criteria) this;
        }

        public Criteria andSolutionEqualTo(String value) {
            addCriterion("solution =", value, "solution");
            return (Criteria) this;
        }

        public Criteria andSolutionNotEqualTo(String value) {
            addCriterion("solution <>", value, "solution");
            return (Criteria) this;
        }

        public Criteria andSolutionGreaterThan(String value) {
            addCriterion("solution >", value, "solution");
            return (Criteria) this;
        }

        public Criteria andSolutionGreaterThanOrEqualTo(String value) {
            addCriterion("solution >=", value, "solution");
            return (Criteria) this;
        }

        public Criteria andSolutionLessThan(String value) {
            addCriterion("solution <", value, "solution");
            return (Criteria) this;
        }

        public Criteria andSolutionLessThanOrEqualTo(String value) {
            addCriterion("solution <=", value, "solution");
            return (Criteria) this;
        }

        public Criteria andSolutionLike(String value) {
            addCriterion("solution like", value, "solution");
            return (Criteria) this;
        }

        public Criteria andSolutionNotLike(String value) {
            addCriterion("solution not like", value, "solution");
            return (Criteria) this;
        }

        public Criteria andSolutionIn(List<String> values) {
            addCriterion("solution in", values, "solution");
            return (Criteria) this;
        }

        public Criteria andSolutionNotIn(List<String> values) {
            addCriterion("solution not in", values, "solution");
            return (Criteria) this;
        }

        public Criteria andSolutionBetween(String value1, String value2) {
            addCriterion("solution between", value1, value2, "solution");
            return (Criteria) this;
        }

        public Criteria andSolutionNotBetween(String value1, String value2) {
            addCriterion("solution not between", value1, value2, "solution");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdIsNull() {
            addCriterion("duplicate_id is null");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdIsNotNull() {
            addCriterion("duplicate_id is not null");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdEqualTo(String value) {
            addCriterion("duplicate_id =", value, "duplicateId");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdNotEqualTo(String value) {
            addCriterion("duplicate_id <>", value, "duplicateId");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdGreaterThan(String value) {
            addCriterion("duplicate_id >", value, "duplicateId");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdGreaterThanOrEqualTo(String value) {
            addCriterion("duplicate_id >=", value, "duplicateId");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdLessThan(String value) {
            addCriterion("duplicate_id <", value, "duplicateId");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdLessThanOrEqualTo(String value) {
            addCriterion("duplicate_id <=", value, "duplicateId");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdLike(String value) {
            addCriterion("duplicate_id like", value, "duplicateId");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdNotLike(String value) {
            addCriterion("duplicate_id not like", value, "duplicateId");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdIn(List<String> values) {
            addCriterion("duplicate_id in", values, "duplicateId");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdNotIn(List<String> values) {
            addCriterion("duplicate_id not in", values, "duplicateId");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdBetween(String value1, String value2) {
            addCriterion("duplicate_id between", value1, value2, "duplicateId");
            return (Criteria) this;
        }

        public Criteria andDuplicateIdNotBetween(String value1, String value2) {
            addCriterion("duplicate_id not between", value1, value2, "duplicateId");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNull() {
            addCriterion("product_id is null");
            return (Criteria) this;
        }

        public Criteria andProductIdIsNotNull() {
            addCriterion("product_id is not null");
            return (Criteria) this;
        }

        public Criteria andProductIdEqualTo(Integer value) {
            addCriterion("product_id =", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotEqualTo(Integer value) {
            addCriterion("product_id <>", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThan(Integer value) {
            addCriterion("product_id >", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("product_id >=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThan(Integer value) {
            addCriterion("product_id <", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdLessThanOrEqualTo(Integer value) {
            addCriterion("product_id <=", value, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdIn(List<Integer> values) {
            addCriterion("product_id in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotIn(List<Integer> values) {
            addCriterion("product_id not in", values, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdBetween(Integer value1, Integer value2) {
            addCriterion("product_id between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andProductIdNotBetween(Integer value1, Integer value2) {
            addCriterion("product_id not between", value1, value2, "productId");
            return (Criteria) this;
        }

        public Criteria andReopenCountIsNull() {
            addCriterion("reopen_count is null");
            return (Criteria) this;
        }

        public Criteria andReopenCountIsNotNull() {
            addCriterion("reopen_count is not null");
            return (Criteria) this;
        }

        public Criteria andReopenCountEqualTo(Integer value) {
            addCriterion("reopen_count =", value, "reopenCount");
            return (Criteria) this;
        }

        public Criteria andReopenCountNotEqualTo(Integer value) {
            addCriterion("reopen_count <>", value, "reopenCount");
            return (Criteria) this;
        }

        public Criteria andReopenCountGreaterThan(Integer value) {
            addCriterion("reopen_count >", value, "reopenCount");
            return (Criteria) this;
        }

        public Criteria andReopenCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("reopen_count >=", value, "reopenCount");
            return (Criteria) this;
        }

        public Criteria andReopenCountLessThan(Integer value) {
            addCriterion("reopen_count <", value, "reopenCount");
            return (Criteria) this;
        }

        public Criteria andReopenCountLessThanOrEqualTo(Integer value) {
            addCriterion("reopen_count <=", value, "reopenCount");
            return (Criteria) this;
        }

        public Criteria andReopenCountIn(List<Integer> values) {
            addCriterion("reopen_count in", values, "reopenCount");
            return (Criteria) this;
        }

        public Criteria andReopenCountNotIn(List<Integer> values) {
            addCriterion("reopen_count not in", values, "reopenCount");
            return (Criteria) this;
        }

        public Criteria andReopenCountBetween(Integer value1, Integer value2) {
            addCriterion("reopen_count between", value1, value2, "reopenCount");
            return (Criteria) this;
        }

        public Criteria andReopenCountNotBetween(Integer value1, Integer value2) {
            addCriterion("reopen_count not between", value1, value2, "reopenCount");
            return (Criteria) this;
        }

        public Criteria andPriorityIsNull() {
            addCriterion("priority is null");
            return (Criteria) this;
        }

        public Criteria andPriorityIsNotNull() {
            addCriterion("priority is not null");
            return (Criteria) this;
        }

        public Criteria andPriorityEqualTo(Byte value) {
            addCriterion("priority =", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityNotEqualTo(Byte value) {
            addCriterion("priority <>", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityGreaterThan(Byte value) {
            addCriterion("priority >", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityGreaterThanOrEqualTo(Byte value) {
            addCriterion("priority >=", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityLessThan(Byte value) {
            addCriterion("priority <", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityLessThanOrEqualTo(Byte value) {
            addCriterion("priority <=", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityIn(List<Byte> values) {
            addCriterion("priority in", values, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityNotIn(List<Byte> values) {
            addCriterion("priority not in", values, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityBetween(Byte value1, Byte value2) {
            addCriterion("priority between", value1, value2, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityNotBetween(Byte value1, Byte value2) {
            addCriterion("priority not between", value1, value2, "priority");
            return (Criteria) this;
        }

        public Criteria andSeverityIsNull() {
            addCriterion("severity is null");
            return (Criteria) this;
        }

        public Criteria andSeverityIsNotNull() {
            addCriterion("severity is not null");
            return (Criteria) this;
        }

        public Criteria andSeverityEqualTo(Byte value) {
            addCriterion("severity =", value, "severity");
            return (Criteria) this;
        }

        public Criteria andSeverityNotEqualTo(Byte value) {
            addCriterion("severity <>", value, "severity");
            return (Criteria) this;
        }

        public Criteria andSeverityGreaterThan(Byte value) {
            addCriterion("severity >", value, "severity");
            return (Criteria) this;
        }

        public Criteria andSeverityGreaterThanOrEqualTo(Byte value) {
            addCriterion("severity >=", value, "severity");
            return (Criteria) this;
        }

        public Criteria andSeverityLessThan(Byte value) {
            addCriterion("severity <", value, "severity");
            return (Criteria) this;
        }

        public Criteria andSeverityLessThanOrEqualTo(Byte value) {
            addCriterion("severity <=", value, "severity");
            return (Criteria) this;
        }

        public Criteria andSeverityIn(List<Byte> values) {
            addCriterion("severity in", values, "severity");
            return (Criteria) this;
        }

        public Criteria andSeverityNotIn(List<Byte> values) {
            addCriterion("severity not in", values, "severity");
            return (Criteria) this;
        }

        public Criteria andSeverityBetween(Byte value1, Byte value2) {
            addCriterion("severity between", value1, value2, "severity");
            return (Criteria) this;
        }

        public Criteria andSeverityNotBetween(Byte value1, Byte value2) {
            addCriterion("severity not between", value1, value2, "severity");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}