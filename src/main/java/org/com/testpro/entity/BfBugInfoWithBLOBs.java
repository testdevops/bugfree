package org.com.testpro.entity;

public class BfBugInfoWithBLOBs extends BfBugInfo {
    private String mailTo;

    private String repeatStep;

    private String modifiedBy;

    public String getMailTo() {
        return mailTo;
    }

    public void setMailTo(String mailTo) {
        this.mailTo = mailTo == null ? null : mailTo.trim();
    }

    public String getRepeatStep() {
        return repeatStep;
    }

    public void setRepeatStep(String repeatStep) {
        this.repeatStep = repeatStep == null ? null : repeatStep.trim();
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy == null ? null : modifiedBy.trim();
    }
}