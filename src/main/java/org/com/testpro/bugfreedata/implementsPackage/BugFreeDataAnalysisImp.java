package org.com.testpro.bugfreedata.implementsPackage;

import cn.hutool.core.date.DateUtil;
import com.testpro.easyrest.Util.ExcelUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.com.testpro.bugfreedata.Dao.BugFreeSelect;
import org.com.testpro.bugfreedata.abstractInterface.BugFreeDataAbstract;
import org.com.testpro.bugfreedata.bean.BugAction;
import org.com.testpro.bugfreedata.bean.BugRatioCount;
import org.com.testpro.bugfreedata.bean.DataTarget;
import org.com.testpro.bugfreedata.bean.project;
import org.com.testpro.entity.BfBugInfo;
import org.com.testpro.entity.BfBugInfoExample;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component("BugFreeDataAnalysisImp")
@ConfigurationProperties(prefix = "bugfree")
@Getter
@Setter
@Slf4j
public class BugFreeDataAnalysisImp extends BugFreeDataAbstract implements InitializingBean {

  private String startTime;
  private String endTime;
  private String filepath;

  @Resource BugFreeSelect bugfreeSelect;

  @Override
  protected List<DataTarget> getTargetListBean() {
    final Date start = DateUtil.parse(startTime, "yyyy-MM-dd");
    final Date end = DateUtil.parse(endTime, "yyyy-MM-dd");
    // 获取对象项目对象信息
    final List<project> allProjectNameAndId = bugfreeSelect.getAllProjectNameAndId();
    List<DataTarget> dataTargets = new ArrayList<>();
    for (project project : allProjectNameAndId) {

      /*【全局BUG】设置项目名称 */
      final String id = project.getId();
      DataTarget dataTarget = new DataTarget();
      dataTarget.setProjectName(project.getName());
      /* 【全局BUG】设置项目的总bug数据 */
      final List<BfBugInfo> allBugSFromID = getAllBugSFromID(id, endTime);

      if (allBugSFromID.size() == 0) {
        continue;
      }
      dataTarget.setBugQuantity(String.valueOf(allBugSFromID.size()));

      /* 【全局BUG】设置项目的reopen数据 */
      final int ReopenBugQuantityCount =
          allBugSFromID.stream()
              .filter(p -> p.getReopenCount() > 0)
              .collect(Collectors.toList())
              .size();
      final double AllBugSize = allBugSFromID.size();
      final Double result = ((double) ReopenBugQuantityCount / AllBugSize) * 100;

      dataTarget.setReopenBugRatio(String.format("%.2f", result));

      /*【全局BUG】reopenBugQuantity 设置总激活数 */
      dataTarget.setReopenBugQuantity(String.valueOf(ReopenBugQuantityCount));

      /* 【全局BUG】设置未关闭缺陷数所有 */
      final int ClosedSize =
          allBugSFromID.stream()
              .filter(b -> b.getBugStatus().equals(CLOSED_STATUS))
              .collect(Collectors.toList())
              .size();
      dataTarget.setNoCloseBugs(String.valueOf(allBugSFromID.size() - ClosedSize));
      dataTarget.setNoShutBugQuantity(String.valueOf(allBugSFromID.size() - ClosedSize));

      /*【全局BUG】设置bug7天没有解决的问题进行分析 */
      final List<BfBugInfo> SevenBugNoShutQuantityList =
          allBugSFromID.stream()
              .filter(b -> b.getBugStatus().equals(ACTIVE_STATUS)) // 状态是开启
              .filter(
                  b ->
                      b.getCreatedAt()
                          .before(getPastDate(-7, DateUtil.parse(endTime, "yyyy-MM-dd"))))
              .collect(Collectors.toList()); // 7天未解决的Bug

      final String SevenBugNoShutQuantity = String.valueOf(SevenBugNoShutQuantityList.size());
      dataTarget.setSevenBugNoShutQuantity(SevenBugNoShutQuantity);

      /*【全局BUG】设置7日未解决的严重bug数 */
      final int SevenBugNoShutHighLevelBugQuantity =
          SevenBugNoShutQuantityList.stream()
              .filter(b -> b.getSeverity() <= 2)
              .collect(Collectors.toList())
              .size();
      dataTarget.setSevenBugNoShutHighLevelBugQuantity(
          String.valueOf(SevenBugNoShutHighLevelBugQuantity));

      /* 【本月BUG】设置本月新增的bug数据 */
      final List<BfBugInfo> bfBugInfos = selectBugQuantity(id, start, end);
      dataTarget.setNewBugQuantity(String.valueOf(bfBugInfos.size()));

      /*【本月BUG】 获取本月激活的bug总数 */
      final List<String> reopenInThisMonth = getReopenInThisMonth(id, startTime, endTime);
      if (reopenInThisMonth.size() > 0) {
        log.error(
            "专项{},激活的BUG数量{} --具体bugID---{}",
            project.getName(),
            reopenInThisMonth.size(),
            reopenInThisMonth.toString());
      }

      dataTarget.setThisMonthBugReopenBusSizeQuantity(String.valueOf(reopenInThisMonth.size()));

      /* 【本月BUG】设置本月回归的相关bug数据 1、bug状态是关闭日期在近期 2、包含本月激活的数据 */
      final String regressionDefectNumber = getRegressionDefectNumber(id, startTime, endTime);
      dataTarget.setThisMonthBugTestsQuantity(regressionDefectNumber);

      /* 【本月BUG】 设置本月回归的相关bug数据分类数据按照 低级 一般 高级 划分 */
      dataTarget = SetBugSeverity(dataTarget, bfBugInfos);

      /* 【本月BUG】设置本月回归的相关bug数据分类数据按照 功能 性能 划分 */
      dataTarget = SetBugFunctionFl(dataTarget, id, project);

      dataTargets.add(dataTarget);
    }

    return dataTargets;
  }
  /**
   * *
   *
   * @description Bug 根据bugType划分为 功能性能等
   * @param dataTarget 目标数据对象
   * @param id 项目ID
   * @param project 项目对象
   * @author testdevops @Date 2019/6/26 14:43
   * @return org.com.testpro.bugfreedata.bean.DataTarget
   * @throws
   * @since V1.0.0
   */
  private DataTarget SetBugFunctionFl(DataTarget dataTarget, String id, project project) {

    String tablename = "bf_ettonbug_" + id;
    List<BugRatioCount> objects;
    try {
      objects = bugfreeSelect.selectBugQuantityCount(id, startTime, endTime, tablename);

    } catch (Exception e) {
      objects = null;

      log.warn("{}------行车不规范,情人两行泪 请添加BugType字段 且包含 功能，界面，性能，数据，用户体验", project.getName());
      dataTarget.setFunctionBugQuantity("0");
      dataTarget.setUiBugQuantity("0");
      dataTarget.setPerformanceBugQuantity("0");
      dataTarget.setDataBugQuantity("0");
      dataTarget.setUserExperienceBugQuantity("0");
    }

    if (objects != null) {
      for (BugRatioCount ratioCount : objects) {

        if (ratioCount.getBugType() == null) {
          continue;
        }
        if (ratioCount.getBugType().equals("功能")) {
          dataTarget.setFunctionBugQuantity(ratioCount.getSize());
          continue;
        }
        if (ratioCount.getBugType().equals("界面")) {
          dataTarget.setUiBugQuantity(ratioCount.getSize());
          continue;
        }
        if (ratioCount.getBugType().equals("性能")) {
          dataTarget.setPerformanceBugQuantity(ratioCount.getSize());
          continue;
        }
        if (ratioCount.getBugType().equals("数据")) {
          dataTarget.setDataBugQuantity(ratioCount.getSize());
          continue;
        }
        if (ratioCount.getBugType().equals("用户体验")) {
          dataTarget.setUserExperienceBugQuantity(ratioCount.getSize());
        }
      }
    }

    return dataTarget;
  }

  /**
   * *
   *
   * @description 根据本月创建是Bug信息分析出 严重等级
   * @param dataTarget
   * @param bfBugInfos
   * @author testdevops @Date 2019/6/26 13:14
   * @return org.com.testpro.bugfreedata.bean.DataTarget
   * @throws
   * @since V1.0.0
   */
  private DataTarget SetBugSeverity(DataTarget dataTarget, List<BfBugInfo> bfBugInfos) {

    final int Count = bfBugInfos.size();
    if (Count > 0) {
      /* * 高风险bug */
      final double highLevelBugQuantity =
          bfBugInfos.stream().filter(b -> b.getSeverity() < 3).collect(Collectors.toList()).size();
      dataTarget.setHighLevelBugQuantity(String.valueOf(highLevelBugQuantity));
      final double HighLevelBugRatio = (highLevelBugQuantity / Count) * 100;
      dataTarget.setHighLevelBugRatio(String.format("%.2f", HighLevelBugRatio));

      /* 一般的bug情况 */
      final double mediumLevelBugQuantity =
          bfBugInfos.stream().filter(b -> b.getSeverity() == 3).collect(Collectors.toList()).size();
      dataTarget.setMediumLevelBugQuantity(String.valueOf(mediumLevelBugQuantity));
      final double mediumLevelBugRatio = (mediumLevelBugQuantity / Count) * 100;
      dataTarget.setMediumLevelBugRatio(String.format("%.2f", mediumLevelBugRatio));

      /* 低级Bug */
      final double lowerLevelBugQuantity =
          bfBugInfos.stream().filter(b -> b.getSeverity() > 3).collect(Collectors.toList()).size();
      dataTarget.setLowerLevelBugQuantity(String.valueOf(lowerLevelBugQuantity));
      final double lowerLevelBugRatio = (lowerLevelBugQuantity / Count) * 100;
      dataTarget.setLowerLevelBugRatio(String.format("%.2f", lowerLevelBugRatio));

    } else {
      dataTarget.setMediumLevelBugQuantity("0");
      dataTarget.setMediumLevelBugRatio("0");
      dataTarget.setLowerLevelBugQuantity("0");
      dataTarget.setLowerLevelBugRatio("0");
      dataTarget.setHighLevelBugQuantity("0");
      dataTarget.setHighLevelBugRatio("0");
    }

    return dataTarget;
  }

  /**
   * *
   *
   * @description 根据ID 和截止时间获取所有的BUG
   * @param id 项目名称
   * @param endTime 截止时间
   * @author testdevops @Date 2019/6/26 10:19
   * @return java.util.List<org.com.testpro.entity.BfBugInfo>
   * @throws
   * @since V1.0.0
   */
  private List<BfBugInfo> getAllBugSFromID(String id, String endTime) {

    return selectBugQuantity(id, DateUtil.parse(endTime, "yyyy-MM-dd"));
  }

  @Override
  protected void WriterExcel(List<DataTarget> targetListBean) throws FileNotFoundException {

    try {
      new ExcelUtil().WriterExcelWithListBean(new File(filepath), targetListBean, DataTarget.class);

    } catch (FileNotFoundException e) {
      new ExcelUtil()
          .WriterExcelWithListBean(new File("C:/workdemo.xlsx"), targetListBean, DataTarget.class);
    }
  }
  /**
   * *
   *
   * @description 日期加减函数
   * @param past 正数表示相加 负数表示相减
   * @param nowDate 基准日期
   * @author testdevops @Date 2019/6/26 11:02
   * @return java.util.Date
   * @throws
   * @since V1.0.0
   */
  private Date getPastDate(int past, Date nowDate) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(nowDate);
    calendar.add(Calendar.DAY_OF_YEAR, past);
    return calendar.getTime();
  }

  @Override
  public void afterPropertiesSet() {
    this.Analysis();
  }

  /**
   * *
   *
   * @description 获取回归缺陷数=本月修改日期状态是关闭状态+本月激活Bug且是open状态
   * @param id 项目ID
   * @param start 开始时间
   * @param end 结束时间
   * @author testdevops @Date 2019/6/19 11:34
   * @return java.lang.String
   * @throws
   * @since V1.0.0
   */
  private String getRegressionDefectNumber(String id, String start, String end) {
    final List<String> Closed_bugIDs =
        bugfreeSelect.selectNoShutBugQuantityList(id, start, end, CLOSED_STATUS); // 这段时间返回测试关闭的bug
    final List<String> reopenIds = getReopenInThisMonth(id, start, end); // 这段时间返回测试激活的bug
    if (reopenIds.size() != 0) {
      // 尝试排除激活bug中已经关闭的数据
      List<String> reOpenNOtClose = new ArrayList<>();
      for (String reopenId : reopenIds) {
        boolean in = false;
        for (String closeBugId : Closed_bugIDs) {
          if (closeBugId.equals(reopenId)) {
            in = true;
            break;
          }
        }
        if (!in) {
          // 如果这个问题不在关闭列表中 说明没有和关闭列表的重复就是有效的数据
          reOpenNOtClose.add(reopenId);
        }
      }
      return String.valueOf(Closed_bugIDs.size() + reOpenNOtClose.size());
    } else return String.valueOf(Closed_bugIDs.size());
  }

  /**
   * *
   *
   * @description 本月激活的Bug数据
   * @param id
   * @param start
   * @param end
   * @author testdevops @Date 2019/6/19 11:41
   * @return java.lang.String
   * @throws
   * @since V1.0.0
   */
  private List<String> getReopenInThisMonth(String id, String start, String end) {

    final List<String> BugIds = bugfreeSelect.selectGetReopenBugIds(id, start, end);
    List<String> targetList = new ArrayList<>();
    for (String BugId : BugIds) {
      final List<BugAction> bugActions =
          bugfreeSelect.selectGetBug_Action_Type(BugId, start, end, ACTIVATED);
      if (bugActions.size() >= 1) {
        targetList.add(BugId);
      }
    }

    return targetList;
  }

  @Resource org.com.testpro.bugfreedata.Dao.Mapper.BfBugInfoMapper BfBugInfoMapper;
  /**
   * *
   *
   * @description 创建项目的所有Bug数据
   * @param id
   * @param EndDate
   * @author testdevops @Date 2019/6/24 17:16
   * @return org.com.testpro.entity.BfBugInfoExample
   * @throws
   * @since V1.0.0
   */
  private List<BfBugInfo> selectBugQuantity(String id, Date EndDate) {
    final BfBugInfoExample bfBugInfoExample = new BfBugInfoExample();
    bfBugInfoExample
        .createCriteria()
        .andProductIdEqualTo(Integer.parseInt(id))
        .andCreatedAtLessThanOrEqualTo(EndDate);
    return BfBugInfoMapper.selectByExample(bfBugInfoExample);
  }

  /**
   * *
   *
   * @description 创建项目的所有Bug数据 根据开始时间和截止时间
   * @param id
   * @param start
   * @param EndDate
   * @author testdevops @Date 2019/6/26 11:34
   * @return java.util.List<org.com.testpro.entity.BfBugInfo>
   * @throws
   * @since V1.0.0
   */
  private List<BfBugInfo> selectBugQuantity(String id, Date start, Date EndDate) {
    final BfBugInfoExample bfBugInfoExample = new BfBugInfoExample();
    bfBugInfoExample
        .createCriteria()
        .andCreatedAtGreaterThanOrEqualTo(start)
        .andProductIdEqualTo(Integer.parseInt(id))
        .andCreatedAtLessThanOrEqualTo(EndDate);
    return BfBugInfoMapper.selectByExample(bfBugInfoExample);
  }
}
