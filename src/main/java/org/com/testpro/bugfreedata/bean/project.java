package org.com.testpro.bugfreedata.bean;

import lombok.Data;

@Data
public class project {

  private String id;

  private String name;

  public final String tableprefix = "bf_ettonresult";

  private String tableName;
}
