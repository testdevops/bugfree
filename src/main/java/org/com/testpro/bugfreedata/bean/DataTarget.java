package org.com.testpro.bugfreedata.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DataTarget extends BaseRowModel {

  @ExcelProperty(index = 0, value = "项目名称")
  private String projectName;

  @ExcelProperty(index = 1, value = "本月回归测试bug数据")
  /** 本月回归测试bug数据 */
  private String thisMonthBugTestsQuantity;

  @ExcelProperty(index = 2, value = "本月激活Bug数")
  private String thisMonthBugReopenBusSizeQuantity;

  @ExcelProperty(index = 3, value = "未关闭缺陷数（所有）")
  private String noCloseBugs;

  @ExcelProperty(index = 4, value = "新增缺陷数(本月)")
  private String newBugQuantity;

  @ExcelProperty(index = 5, value = "未关闭缺陷数（所有）")
  private String noShutBugQuantity;

  @ExcelProperty(index = 6, value = "7日未解决Bug")
  private String sevenBugNoShutQuantity;

  @ExcelProperty(index = 7, value = "7日未解决Bug 且严重")
  private String sevenBugNoShutHighLevelBugQuantity;

  @ExcelProperty(index = 8, value = "低级bug总数")
  private String lowerLevelBugQuantity;

  @ExcelProperty(index = 9, value = "低级bug缺陷率")
  private String lowerLevelBugRatio;

  @ExcelProperty(index = 10, value = "一般bug总数")
  private String mediumLevelBugQuantity;

  @ExcelProperty(index = 11, value = "一般bug缺陷率")
  private String mediumLevelBugRatio;

  @ExcelProperty(index = 12, value = "高级bug总数")
  private String highLevelBugQuantity;

  @ExcelProperty(index = 13, value = "高级bug缺陷率")
  private String highLevelBugRatio;

  @ExcelProperty(index = 14, value = "总激活数")
  private String reopenBugQuantity;

  @ExcelProperty(index = 15, value = "bug总数")
  private String bugQuantity;

  @ExcelProperty(index = 16, value = "总reopen率")
  private String reopenBugRatio;

  @ExcelProperty(index = 17, value = "功能问题")
  private String functionBugQuantity;

  @ExcelProperty(index = 18, value = "界面问题")
  private String uiBugQuantity;

  @ExcelProperty(index = 19, value = "性能问题")
  private String performanceBugQuantity;

  @ExcelProperty(index = 20, value = "数据问题")
  private String dataBugQuantity;

  @ExcelProperty(index = 21, value = "用户体验")
  private String userExperienceBugQuantity;
}
