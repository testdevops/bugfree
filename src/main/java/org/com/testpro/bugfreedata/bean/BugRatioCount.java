package org.com.testpro.bugfreedata.bean;

import lombok.Data;

@Data
public class BugRatioCount {
  private String size;

  private String severity;

  private String bug_severity;

  private String BugType;
}
