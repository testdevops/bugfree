package org.com.testpro.bugfreedata.bean;

import lombok.Data;

import java.util.Date;

/**
 * @author Administrator testdevops
 * @description Bug执行记录对象
 * @create 2019-06-19 12:36
 */
@Data
public class BugAction {
  private String id;
  private Date created_at;
  private String created_by;
  private String action_type;
  private String action_note;
  private String buginfo_id;
}
