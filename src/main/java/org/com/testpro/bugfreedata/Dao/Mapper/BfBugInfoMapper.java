package org.com.testpro.bugfreedata.Dao.Mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.com.testpro.entity.BfBugInfo;
import org.com.testpro.entity.BfBugInfoExample;
import org.com.testpro.entity.BfBugInfoWithBLOBs;

public interface BfBugInfoMapper {
    int countByExample(BfBugInfoExample example);

    int deleteByExample(BfBugInfoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(BfBugInfoWithBLOBs record);

    int insertSelective(BfBugInfoWithBLOBs record);

    List<BfBugInfoWithBLOBs> selectByExampleWithBLOBs(BfBugInfoExample example);

    List<BfBugInfo> selectByExample(BfBugInfoExample example);

    BfBugInfoWithBLOBs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") BfBugInfoWithBLOBs record, @Param("example") BfBugInfoExample example);

    int updateByExampleWithBLOBs(@Param("record") BfBugInfoWithBLOBs record, @Param("example") BfBugInfoExample example);

    int updateByExample(@Param("record") BfBugInfo record, @Param("example") BfBugInfoExample example);

    int updateByPrimaryKeySelective(BfBugInfoWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(BfBugInfoWithBLOBs record);

    int updateByPrimaryKey(BfBugInfo record);
}