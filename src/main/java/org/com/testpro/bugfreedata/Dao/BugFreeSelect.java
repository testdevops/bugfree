package org.com.testpro.bugfreedata.Dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.com.testpro.bugfreedata.bean.BugAction;
import org.com.testpro.bugfreedata.bean.BugRatioCount;
import org.com.testpro.bugfreedata.bean.project;

import java.util.List;

public interface BugFreeSelect {

  /**
   * @description 总的bug数据
   * @param id project_Id
   * @param end 截止时间
   * @author testdevops
   * @date 2019-05-17 12:46
   * @return java.lang.String
   * @throws
   * @since V 1.0
   */
  @Select(
      "SELECT COUNT(1) FROM  bf_bug_info LEFT JOIN bf_product  ON bf_bug_info.`product_id` =bf_product.id  WHERE bf_product.`id`=#{id} and bf_bug_info.`created_at` <=#{end}")
  String selectBugQuantity(String id, String end);

  /**
   * @description 全部的项目列表
   * @author testdevops
   * @date 2019-05-17 12:49
   * @return java.util.List<org.com.testpro.bugfreedata.bean.project>
   * @throws
   * @since V 1.0
   */
  @Select("select * from bf_product ")
  List<project> getAllProjectNameAndId();

  @Select(
      "SELECT bf_bug_info.`id` FROM  bf_bug_info LEFT JOIN bf_product  ON bf_bug_info.`product_id` =bf_product.id WHERE  bf_product.`id`=#{id} and  bf_bug_info.bug_status=#{bug_status}")
  List<String> GetBugNoCloseList(String id, String bug_status);
  /**
   * @description 7日未关闭bug总数
   * @param id project_Id
   * @param end 截止时间
   * @author testdevops
   * @date 2019-05-17 12:50
   * @return java.lang.String
   * @throws
   * @since V 1.0
   */
  @Select(
      "SELECT  COUNT(1) FROM  bf_bug_info   LEFT JOIN bf_product  ON bf_bug_info.`product_id` =bf_product.id \n"
          + "WHERE bf_bug_info.`bug_status` = \"Active\" AND  DATEDIFF(DATE_FORMAT(#{end},'%Y-%m-%d'),DATE_FORMAT(bf_bug_info.`created_at`,'%Y-%m-%d'))>7  AND bf_product.id=#{id}")
  String selectSevenBugNoShutQuantity(String id, String end);

  /**
   * *
   *
   * @description 7日未关闭bug且严重等级
   * @param id project_Id
   * @param end 截止时间
   * @author testdevops @Date 2019/6/21 18:03
   * @return java.lang.String
   * @throws
   * @since V1.0.0
   */
  @Select(
      "SELECT  COUNT(1) FROM  bf_bug_info   LEFT JOIN bf_product  ON bf_bug_info.`product_id` =bf_product.id \n"
          + "WHERE bf_bug_info.`bug_status` = \"Active\" AND  DATEDIFF(DATE_FORMAT(#{end},'%Y-%m-%d'),DATE_FORMAT(bf_bug_info.`created_at`,'%Y-%m-%d'))>7  AND bf_product.id=#{id} AND bf_bug_info.severity<= 2")
  String selectSevenBugNoShutHighLevelBugQuantity(String id, String end);
  /**
   * @description 新增Bug总数
   * @param id project_Id
   * @param start 开始时间 2019-05-01
   * @param end 结束时间 2019-05-10
   * @author testdevops
   * @date 2019-05-17 12:51
   * @return java.lang.String
   * @throws
   * @since V 1.0
   */
  @Select(
      "SELECT COUNT(1) FROM  bf_bug_info LEFT JOIN bf_product  ON bf_bug_info.`product_id` =bf_product.id  WHERE bf_product.`id`=#{id} and  bf_bug_info.`created_at` BETWEEN  DATE_FORMAT(#{start,jdbcType=VARCHAR},'%Y-%m-%d') AND DATE_FORMAT(#{end,jdbcType=VARCHAR},'%Y-%m-%d') ")
  String selectNewBugQuantity(
      @Param("id") String id, @Param("start") String start, @Param("end") String end);

  /**
   * @description 未关闭Bug总数
   * @param id project_Id
   * @author testdevops
   * @date 2019-05-17 12:53
   * @return java.lang.String
   * @throws
   * @since V 1.0
   */
  @Select(
      "SELECT COUNT(1) FROM  bf_bug_info LEFT JOIN bf_product  ON bf_bug_info.`product_id` =bf_product.id  WHERE bf_product.`id`=#{id} and bf_bug_info.`bug_status` != \"Closed\" ")
  String selectNoShutBugQuantity(String id);

  /**
   * *
   *
   * @description 查询规定关闭时间内的Bug状态情况
   * @param id
   * @param start
   * @param end
   * @param BugStatus
   * @author testdevops @Date 2019/6/19 13:43
   * @return java.util.List<java.lang.String>
   * @throws
   * @since V1.0.0
   */
  @Select(
      "SELECT bf_bug_info.id FROM  bf_bug_info LEFT JOIN bf_product  ON bf_bug_info.`product_id` =bf_product.id  WHERE bf_product.`id`=#{id} and  bf_bug_info.`closed_at` BETWEEN  DATE_FORMAT(#{start,jdbcType=VARCHAR},'%Y-%m-%d') AND DATE_FORMAT(#{end,jdbcType=VARCHAR},'%Y-%m-%d') and bug_status =#{BugStatus}")
  List<String> selectNoShutBugQuantityList(
      @Param("id") String id,
      @Param("start") String start,
      @Param("end") String end,
      @Param("BugStatus") String BugStatus);

  /**
   * @description 统计对应项目的所有reopenBug总数
   * @param id project_Id
   * @author testdevops
   * @date 2019-05-17 12:55
   * @return java.lang.String
   * @throws
   * @since V 1.0
   */
  @Select(
      "SELECT COUNT(1) FROM  bf_bug_info LEFT JOIN bf_product  ON bf_bug_info.`product_id` =bf_product.id  WHERE bf_product.`id`= #{id} and bf_bug_info.`reopen_count`>0")
  String selectReopenBugQuantityCount(String id);

  /**
   * @description 根据严重程度统计数量和严重等级分布（一定时间段内）
   * @param id project_Id
   * @param start 开始时间 2019-05-01
   * @param end 结束时间 2019-05-10
   * @author testdevops
   * @date 2019-05-17 12:56
   * @return java.util.List<org.com.testpro.bugfreedata.bean.BugRatioCount>
   * @throws
   * @since V 1.0
   */
  @Select(
      "SELECT COUNT(1)AS size,bf_bug_info.`severity`,bf_product.`bug_severity` FROM  bf_bug_info LEFT JOIN bf_product  ON bf_bug_info.`product_id` =bf_product.id  WHERE bf_product.`id`= #{id} and bf_bug_info.`created_at` BETWEEN  DATE_FORMAT(#{start,jdbcType=VARCHAR},'%Y-%m-%d') AND DATE_FORMAT(#{end,jdbcType=VARCHAR},'%Y-%m-%d')  GROUP BY bf_bug_info.`severity`")
  List<BugRatioCount> selectBugRatioCount(
      String id, @Param("start") String start, @Param("end") String end);

  /**
   * @description 根据问题类型统计数量和问题类型分布（一定时间段内）
   * @param id project_Id
   * @param start 开始时间 2019-05-01
   * @param end 结束时间 2019-05-10
   * @param tableName 表名称
   * @author testdevops
   * @date 2019-05-17 12:58
   * @return java.util.List<org.com.testpro.bugfreedata.bean.BugRatioCount>
   * @throws
   * @since V 1.0
   */
  @Select(
      "SELECT  COUNT(*)AS size,${tableName}.`BugType` FROM  bf_bug_info   LEFT JOIN bf_product  ON bf_bug_info.`product_id` =bf_product.id LEFT JOIN ${tableName}  ON bf_bug_info.`id`= ${tableName}.`bug_id`\n"
          + "WHERE    bf_bug_info.`product_id`=#{id}  and bf_bug_info.`created_at` BETWEEN  DATE_FORMAT(#{start,jdbcType=VARCHAR},'%Y-%m-%d') AND DATE_FORMAT(#{end,jdbcType=VARCHAR},'%Y-%m-%d') GROUP BY ${tableName}.`BugType` ")
  List<BugRatioCount> selectBugQuantityCount(
      String id,
      @Param("start") String start,
      @Param("end") String end,
      @Param("tableName") String tableName);

  /**
   * *
   *
   * @description 查询关于reopen>=1且修改日期是本月内的数据bug id（有可能激活时间是上个月）
   * @param id project_Id
   * @param start 开始时间 2019-05-01
   * @param end 结束时间 2019-05-10
   * @author testdevops @Date 2019/6/19 12:19
   * @return java.util.List<java.lang.String>
   * @throws
   * @since V1.0.0
   */
  @Select(
      "SELECT bf_bug_info.id FROM  bf_bug_info LEFT JOIN bf_product  ON bf_bug_info.`product_id` =bf_product.id  WHERE bf_product.`id`=#{id} and  bf_bug_info.`updated_at` BETWEEN  DATE_FORMAT(#{start,jdbcType=VARCHAR},'%Y-%m-%d') AND DATE_FORMAT(#{end,jdbcType=VARCHAR},'%Y-%m-%d') and reopen_count>=1")
  List<String> selectGetReopenBugIds(
      @Param("id") String id, @Param("start") String start, @Param("end") String end);

  /**
   * @param BugId 问题ID
   * @param start 开始时间
   * @param end 结束时间
   * @param actionType 动作类型 action_type --------------- activated closed closed_edit opened
   *     opened_edit resolved resolved_edit
   * @return
   */
  @Select(
      "SELECT * FROM  `bf_bug_action` WHERE buginfo_id =#{BugId} AND created_at BETWEEN  DATE_FORMAT(#{start,jdbcType=VARCHAR},'%Y-%m-%d') AND DATE_FORMAT(#{end,jdbcType=VARCHAR},'%Y-%m-%d') AND action_type =#{actionType} ORDER BY created_at DESC")
  List<BugAction> selectGetBug_Action_Type(
      @Param("BugId") String BugId,
      @Param("start") String start,
      @Param("end") String end,
      @Param("actionType") String actionType);

  //  List<String> SelectUpdateTimeBugIDs();
}
