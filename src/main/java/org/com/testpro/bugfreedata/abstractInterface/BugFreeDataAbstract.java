package org.com.testpro.bugfreedata.abstractInterface;

import org.com.testpro.bugfreedata.Interface.DataAnalysis;
import org.com.testpro.bugfreedata.bean.DataTarget;

import java.io.FileNotFoundException;
import java.util.List;

public abstract class BugFreeDataAbstract implements DataAnalysis {
  /** 激活动作 */
  public static final String ACTIVATED = "activated";
  /** 关闭动作 */
  public static final String CLOSED = "closed";
  /** 关闭编辑动作 */
  public static final String CLOSED_EDIT = "closed_edit";
  /** 创建动作 */
  public static final String OPENED = "opened";
  /** 开启状态编辑 */
  public static final String OPENED_EDIT = "opened_edit";
  /** 解决动作 */
  public static final String RESOLVED = "resolved";
  /** 开启状态 */
  public static final String ACTIVE_STATUS = "Active";
  /** 关闭状态 */
  public static final String CLOSED_STATUS = "Closed";
  /** 解决状态 */
  public static final String RESOLVED_STATUS = "Resolved";

  @Override
  public void Analysis() {
    List<DataTarget> targetListBean = getTargetListBean();
    try {
      WriterExcel(targetListBean);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  protected abstract List<DataTarget> getTargetListBean();

  protected abstract void WriterExcel(List<DataTarget> targetListBean) throws FileNotFoundException;
}
