import org.com.testpro.SpringBootApp;
import org.com.testpro.bugfreedata.Interface.DataAnalysis;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import javax.annotation.Resource;

@SpringBootTest(
    classes = {SpringBootApp.class},
    webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class tests extends AbstractTestNGSpringContextTests {
  @Resource(name = "BugFreeDataAnalysisImp")
  DataAnalysis dataAnalysis;

  @Test
  public void tests() {
    dataAnalysis.Analysis();
  }
}
