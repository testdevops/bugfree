# BugFree质量统计数据

#### 项目介绍
 这个是为了在使用bugfree平台时能够统计月度相关数据
#### 源码使用方法
   - 源码配置application.properties 中bugFree中的数据库信息
   - 配置 application.properties开始结束时间
   - 配置 bugFree中添加自定义字段BugType 且包含 功能，界面，性能，数据，用户体验"
   - 配置 bugFree中 [Bug严重程度] 致命,严重,一般,轻微,建议
   - 执行/src/test/java/tests.java
   
#### RELEASE使用方法
   - 源码配置application.properties 中bugFree中的数据库信息
   - 配置 application.properties开始结束时间
   - 配置 bugFree中添加自定义字段BugType 且包含 功能，界面，性能，数据，用户体验"
   - 配置 bugFree中 [Bug严重程度] 致命,严重,一般,轻微,建议
   - 执行/burfreeTest/bin/start.bat